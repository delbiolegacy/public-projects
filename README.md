Installare ambiente sviluppo Progetti Pubblici
==============================================

Su macchine Linux e Mac OSX eseguire il seguente codice, per inizializzare l'ambiente

###Per macchine Linux:

    make install && source ~/.bashrc && vim ~/.bashrc

###Mentre Mac OSX

    make install && source ~/.bash_profile && vim ~/.bash_profile
    
####PS: ricorda di eseguire

    git checkout master

all'interno di ogni submodule dei progetti git importati
