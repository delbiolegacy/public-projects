UNAME := $(shell uname)


bash_linux = $(HOME)/.bashrc
bash_mac = $(HOME)/.bash_profile

ifeq ($(UNAME),Linux)
	bash_file=$(bash_linux)
endif

ifeq ($(UNAME),Darwin)
	bash_file=$(bash_mac)
endif

install-sdk:
	git submodule update --init

update-sdk:
	git pull
	git submodule sync
	git submodule update --init	

install: install-sdk
	$(MAKE) alias

update: update-sdk
	#$(MAKE) manage_vm update

clear:
	#rm -rfv manage_vm

alias:
	@echo "#-------------------------------" >> $(bash_file)
	@echo "# Public Project Aliases" >> $(bash_file)
	@echo "#-------------------------------" >> $(bash_file)
	# UPDATE PRIVATE PROJECTS
	@echo "alias update_public='make -C $(PWD) update'" >> $(bash_file)




# PATTERN FOR ALIASES: @echo "" >> $(bash_file)

